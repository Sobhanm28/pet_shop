import 'package:flutter/material.dart';
import 'package:pet_shop/core/services/navigation_service.dart';
import 'package:pet_shop/locator.dart';
import 'package:pet_shop/ui/screen/detail/detail_screen.dart';
import 'package:pet_shop/ui/screen/discover/discover_screen.dart';
import 'package:pet_shop/ui/screen/home/home_screen.dart';

void main() {
  setupLocator();
  return runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      navigatorKey: locator<NavigationService>().navigationKey,
      theme: ThemeData(
        fontFamily: "mont",
        accentColor: Colors.blue,
        primaryColor: Colors.red,
      ),
      initialRoute: "discover",
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case "discover":
            return MaterialPageRoute(builder: (context) => DiscoverScreen());
            break;
          case "home":
            return MaterialPageRoute(builder: (context) => HomeScreen());
            break;
          case "detail":
            return PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 1000),
              pageBuilder: (_, __, ___) => DetailScreen(
                args: settings.arguments,
              ),
            );
            break;
          default:
            MaterialPageRoute(builder: (context) => HomeScreen());
        }
      },
      home: DetailScreen(),
    );
  }
}
