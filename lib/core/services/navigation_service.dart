import 'package:flutter/cupertino.dart';
import 'package:pet_shop/ui/model/pet.dart';

class NavigationService {
  GlobalKey<NavigatorState> navigationKey = GlobalKey<NavigatorState>();

  Future pushReplaceNamed(String routeName) {
    return navigationKey.currentState.pushReplacementNamed(routeName);
  }

  Future pushNamed({String routeName, Map args}) {
    return navigationKey.currentState.pushNamed(routeName, arguments: args);
  }

  void goBack(String routeName) {
    return navigationKey.currentState.pop();
  }
}
