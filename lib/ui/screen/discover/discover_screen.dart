import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:pet_shop/ui/screen/discover/discover_widgets.dart';

class DiscoverScreen extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final animalCategoryStreamController = useStreamController<int>();
    final backgroundAnimationController =
        useAnimationController(duration: Duration(seconds: 10));

    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Stack(
          children: [
            DiscoverBackground(
              context: context,
              controller: backgroundAnimationController,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                DiscoverTitle(
                    title: "Discover",
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.black87,
                    marginLTRB: [0, 0, 0, 0]),
                CategoryViewer(
                  context: context,
                  streamController: animalCategoryStreamController,
                ),
                PageIndicator(streamController: animalCategoryStreamController),
                DiscoverTitle(
                    title: "Make a new friend",
                    fontSize: 18,
                    color: Colors.black87,
                    fontWeight: FontWeight.normal,
                    marginLTRB: [0, 0, 0, 0]),
                DiscoverTitle(
                  title:
                      "Discover all popular breeds,\nLearn how to care for your lovely pet.",
                  fontSize: 14,
                  color: Colors.grey,
                  fontWeight: FontWeight.normal,
                  marginLTRB: [0, 10, 0, 40],
                ),
                DiscoverButton(
                  title: "Get Started",
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
