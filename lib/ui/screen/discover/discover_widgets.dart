import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pet_shop/core/services/navigation_service.dart';
import 'package:pet_shop/locator.dart';

class DiscoverTitle extends StatelessWidget {
  final String title;
  final double fontSize;
  final FontWeight fontWeight;
  final Color color;
  final List<double> marginLTRB;

  const DiscoverTitle(
      {Key key,
      this.title,
      this.fontSize,
      this.fontWeight,
      this.color,
      this.marginLTRB})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(
          marginLTRB[0], marginLTRB[1], marginLTRB[2], marginLTRB[3]),
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: fontSize,
          fontWeight: fontWeight,
          color: color,
        ),
      ),
    );
  }
}

List animalsImage = [
  'assets/images/rabbit_cat.jpg',
  'assets/images/cat_cat.jpg',
  'assets/images/dog1_cat.jpg',
  'assets/images/parrot_cat.jpg',
];

width(double d, {@required BuildContext context}) {
  return MediaQuery.of(context).size.width * d;
}

height(double d, {@required BuildContext context}) {
  return MediaQuery.of(context).size.height * d;
}

class DiscoverBackground extends StatelessWidget {
  final BuildContext context;
  final AnimationController controller;

  const DiscoverBackground({Key key, this.context, this.controller})
      : super(key: key);

  backgroundCircle({
    double radius,
    List<double> beginLTRB,
    List<double> endLTRB,
    Color color,
  }) {
    return PositionedTransition(
      rect: react(beginLTRB: beginLTRB, endLTRB: endLTRB),
      child: Center(
        child: Container(
          width: radius,
          height: radius,
          decoration: BoxDecoration(color: color, shape: BoxShape.circle),
        ),
      ),
    );
  }

  Animation<RelativeRect> react(
          {List<double> beginLTRB, List<double> endLTRB}) =>
      RelativeRectTween(
              begin: RelativeRect.fromLTRB(
                  beginLTRB[0], beginLTRB[1], beginLTRB[2], beginLTRB[3]),
              end: RelativeRect.fromLTRB(
                  endLTRB[0], endLTRB[1], endLTRB[2], endLTRB[3]))
          .animate(controller);

  @override
  Widget build(BuildContext context) {
    controller.forward();
    controller.addStatusListener((status) {
      print("$status");
      switch (status) {
        case AnimationStatus.completed:
          controller.reverse();
          break;
        case AnimationStatus.dismissed:
          controller.forward();
          break;
        default:
      }
    });

    return ScaleTransition(
      scale: Tween<double>(begin: 2, end: 2.5).animate(controller),
      child: Container(
        width: width(1, context: context),
        height: height(1, context: context),
        child: Stack(
          children: [
            backgroundCircle(
              radius: 100,
              beginLTRB: [0, 0, 0, 200],
              endLTRB: [0, 0, 80, 200],
              color: Colors.pink[50],
            ),
            backgroundCircle(
              radius: 80,
              beginLTRB: [80, 0, 0, 0],
              endLTRB: [100, 70, 0, 0],
              color: Colors.orangeAccent[100],
            ),
            backgroundCircle(
              radius: 40,
              beginLTRB: [0, 280, 20, 0],
              endLTRB: [0, 200, 110, 0],
              color: Colors.green[100],
            ),
            backgroundCircle(
              radius: 50,
              beginLTRB: [200, 200, 0, 0],
              endLTRB: [20, 250, 0, 0],
              color: Colors.purple[100],
            ),
          ],
        ),
      ),
    );
  }
}

class CategoryBox extends StatelessWidget {
  final int index;
  final int currentIndex;
  final BuildContext context;
  final String imagePath;
  final AnimationController scaleController;

  const CategoryBox(
      {Key key,
      this.index,
      this.currentIndex,
      this.context,
      this.imagePath,
      this.scaleController})
      : super(key: key);

  dynamic setCondition(firstC, secondC) {
    return index == currentIndex ? firstC : secondC;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: AnimatedContainer(
        width: setCondition(
            width(0.7, context: context), width(0.45, context: context)),
        height: setCondition(
            height(0.425, context: context), height(0.35, context: context)),
        child: Stack(
          children: [
            Center(
              child: ClipRRect(
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
                child: Image.asset(
                  imagePath,
                  scale: 2.4,
                  width: width(1, context: context),
                  height: height(1, context: context),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
                gradient: LinearGradient(
                    colors: [Colors.black12, Colors.black26, Colors.black87],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter),
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
            // image: DecorationImage(
            //     image: AssetImage(imagePath), fit: BoxFit.cover),
            border: Border.all(color: Colors.black, width: 2)),
        duration: Duration(milliseconds: 1200),
        curve: Curves.fastLinearToSlowEaseIn,
      ),
    );
  }
}

class CategoryViewer extends StatelessWidget {
  final BuildContext context;
  final StreamController streamController;

  const CategoryViewer({Key key, this.context, this.streamController})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
      stream: streamController.stream,
      initialData: 0,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          width: width(1, context: context),
          height: height(0.5, context: context),
          child: PageView.builder(
            onPageChanged: (index) {
              print(index);
              streamController.sink.add(index);
            },
            controller: PageController(viewportFraction: 0.6),
            itemCount: animalsImage.length,
            itemBuilder: (context, index) => CategoryBox(
              index: index,
              currentIndex: snapshot.data,
              context: context,
              imagePath: animalsImage[index],
            ),
          ),
        );
      },
    );
  }
}

class PageIndicator extends StatelessWidget {
  final StreamController streamController;

  const PageIndicator({Key key, this.streamController}) : super(key: key);

  circleShape({int index, int currentIndex}) {
    dynamic setCondition(firstC, secondC) {
      return index == currentIndex ? firstC : secondC;
    }

    return Container(
      margin: EdgeInsets.fromLTRB(2, 0, 2, 30),
      width: 8,
      height: 8,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: setCondition(Colors.black, Colors.grey),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: StreamBuilder(
        stream: streamController.stream,
        initialData: 0,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(
              animalsImage.length,
              (index) => circleShape(index: index, currentIndex: snapshot.data),
            ),
          );
        },
      ),
    );
  }
}

class DiscoverButton extends StatelessWidget {
  final String title;

  const DiscoverButton({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        locator<NavigationService>().pushReplaceNamed("home");
      },
      child: Container(
        width: 250,
        height: 45,
        alignment: Alignment.center,
        child: Text(
          title,
          style: TextStyle(color: Colors.white),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(30)),
            color: Colors.black87),
      ),
    );
  }
}
