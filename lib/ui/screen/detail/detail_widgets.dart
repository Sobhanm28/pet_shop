import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pet_shop/ui/model/pet.dart';
import 'package:pet_shop/ui/screen/discover/discover_widgets.dart';

List charactertics = [
  "Kid frendly",
  "friendly towards stranger",
  "tole rates cold weather",
  "Kid frendly",
  "friendly towards stranger",
  "lorem ipsum ",
  "tole rates cold weather",
];

List propertyColorPet = [
  "Breed",
  "Age",
  "Sex",
  "Color",
];

Map detailArgs;

List tagsColor(colorNumber) => [
      Colors.red[colorNumber],
      Colors.green[colorNumber],
      Colors.brown[colorNumber],
      Colors.indigo[colorNumber],
      Colors.orange[colorNumber],
      Colors.yellow[colorNumber],
      Colors.amber[colorNumber],
      Colors.blue[colorNumber],
    ];

class DetailPet extends StatelessWidget {
  final Pet pet;
  const DetailPet({
    Key key,
    this.pet,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        padding: EdgeInsets.fromLTRB(20, 40, 20, 0),
        height: height(0.55, context: context),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(30),
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              TitleDetail(
                pet: pet,
              ),
              AboutDetail(),
              PropertyList(props: [pet.breed, pet.age, pet.sex, pet.color]),
              CharacteristecTags()
            ],
          ),
        ),
      ),
    );
  }
}

class TitleDetail extends StatelessWidget {
  final Pet pet;
  const TitleDetail({
    Key key,
    this.pet,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          pet.breed,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 17,
          ),
        ),
        Text(
          "\$${pet.price}",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 17,
          ),
        ),
      ],
    );
  }
}

class AboutDetail extends StatelessWidget {
  const AboutDetail({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Text(
              "About",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eu vestibulum quam. Mauris massa nulla, placerat id mi in, tempus mattis libero. Etiam sed lectus id augue")
        ],
      ),
    );
  }
}

class CharacteristecTags extends StatelessWidget {
  const CharacteristecTags({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Text(
              "Characteristics",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Wrap(
            children: List.generate(
              charactertics.length,
              (index) {
                int backgrounColorNumber = 100;
                int fontColorNumber = 700;
                int notCare = 100;
                final randomNumber =
                    Random().nextInt(tagsColor(notCare).length);
                setColor(colorNumber) {
                  return tagsColor(colorNumber)[randomNumber];
                }

                return Container(
                  margin: EdgeInsets.all(5),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: setColor(backgrounColorNumber),
                    borderRadius: BorderRadius.all(
                      Radius.circular(15),
                    ),
                  ),
                  child: Text(
                    charactertics[index],
                    style: TextStyle(
                      color: setColor(fontColorNumber),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class PropertyList extends StatelessWidget {
  final List props;
  const PropertyList({
    Key key,
    this.props,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: List.generate(
            4,
            (index) => PropertyItem(
              title: propertyColorPet[index],
              subtitle: props[index],
            ),
          ),
        ),
      ),
    );
  }
}

class PropertyItem extends StatelessWidget {
  final String title;
  final String subtitle;
  const PropertyItem({
    Key key,
    this.title,
    this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width(0.2, context: context),
      height: height(0.1, context: context),
      padding: EdgeInsets.symmetric(vertical: 10),
      margin: EdgeInsets.symmetric(horizontal: 3),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.normal,
              color: Colors.grey,
            ),
          ),
          Text(
            subtitle,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: Colors.grey[200],
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
      ),
    );
  }
}

class DetailAppBar extends StatelessWidget {
  const DetailAppBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 40,
      left: 20,
      child: Icon(
        Icons.arrow_back_ios,
      ),
    );
  }
}

class PetBanner extends StatelessWidget {
  final Pet pet;
  const PetBanner({
    Key key,
    this.pet,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0,
      left: 0,
      right: 0,
      bottom: 250,
      child: Center(
        child: Hero(
          tag: pet.breed,
          child: Material(
            color: Colors.transparent,
            child: Container(
              height: height(0.4, context: context),
              width: width(1, context: context),
              child: Image.asset(
                pet.image,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Background extends StatelessWidget {
  final Color backgroundColor;
  Background({
    Key key,
    this.backgroundColor,
  }) : super(key: key);

  Color shapeColor = Colors.white24;

  Positioned backgroundShape(
      {List<double> postitionFromLTBR,
      Widget widget,
      @required double angleRotate}) {
    return Positioned(
      left: postitionFromLTBR[0],
      top: postitionFromLTBR[1],
      bottom: postitionFromLTBR[2],
      right: postitionFromLTBR[3],
      child: Center(
        child: Transform.rotate(
            angle: angleRotate, child: Container(child: widget)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: 1.5,
      child: Container(
        width: width(1, context: context),
        height: height(1, context: context),
        color: backgroundColor,
        child: Stack(
          children: [
            backgroundShape(
              postitionFromLTBR: [230, 0, 390, 0],
              angleRotate: 0,
              widget: Icon(
                Icons.star,
                size: 40,
                color: shapeColor,
              ),
            ),
            backgroundShape(
              postitionFromLTBR: [200, 0, 0, 0],
              angleRotate: 0,
              widget: Container(
                width: 200,
                height: 200,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    width: 3,
                    color: shapeColor,
                  ),
                ),
              ),
            ),
            backgroundShape(
              postitionFromLTBR: [0, 0, 450, 40],
              angleRotate: 0,
              widget: Icon(
                Icons.circle,
                size: 150,
                color: shapeColor,
              ),
            ),
            backgroundShape(
              postitionFromLTBR: [0, 0, 180, 40],
              angleRotate: 0,
              widget: Icon(
                Icons.circle,
                size: 90,
                color: shapeColor,
              ),
            ),
            backgroundShape(
              postitionFromLTBR: [100, 0, 310, 0],
              angleRotate: 0,
              widget: Icon(
                Icons.circle,
                size: 40,
                color: shapeColor,
              ),
            ),
            backgroundShape(
              postitionFromLTBR: [0, 0, 380, 250],
              angleRotate: -10,
              widget: Icon(
                Icons.pets,
                size: 15,
                color: shapeColor,
              ),
            ),
            backgroundShape(
              postitionFromLTBR: [0, 0, 330, 220],
              angleRotate: 110,
              widget: Icon(
                Icons.pets,
                size: 15,
                color: shapeColor,
              ),
            ),
            backgroundShape(
              postitionFromLTBR: [0, 0, 280, 250],
              angleRotate: 110,
              widget: Icon(
                Icons.pets,
                size: 15,
                color: shapeColor,
              ),
            ),
            backgroundShape(
              postitionFromLTBR: [0, 0, 220, 230],
              angleRotate: 110,
              widget: Icon(
                Icons.pets,
                size: 15,
                color: shapeColor,
              ),
            ),
            backgroundShape(
              postitionFromLTBR: [0, 0, 160, 250],
              angleRotate: 27.9,
              widget: Icon(
                Icons.pets,
                size: 15,
                color: shapeColor,
              ),
            ),
            backgroundShape(
              postitionFromLTBR: [0, 0, 120, 200],
              angleRotate: -10,
              widget: Icon(
                Icons.pets,
                size: 15,
                color: shapeColor,
              ),
            ),
            backgroundShape(
              postitionFromLTBR: [0, 0, 60, 230],
              angleRotate: -10,
              widget: Icon(
                Icons.pets,
                size: 15,
                color: shapeColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
