import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:pet_shop/ui/model/pet.dart';
import 'package:pet_shop/ui/screen/discover/discover_widgets.dart';

import 'detail_widgets.dart';

class DetailScreen extends HookWidget {
  final Map args;

  DetailScreen({this.args});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Stack(
          children: [
            Background(
              backgroundColor: args["backgroundColor"],
            ),
            DetailAppBar(),
            DetailPet(
              pet: args["pet"],
            ),
            PetBanner(
              pet: args["pet"],
            ),
          ],
        ),
      ),
    );
  }
}
