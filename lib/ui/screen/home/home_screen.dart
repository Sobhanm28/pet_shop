import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:pet_shop/ui/enums/enum.dart';
import 'package:pet_shop/ui/screen/discover/discover_widgets.dart';
import 'package:pet_shop/ui/screen/home/home_drawer.dart';

import 'home_widgets.dart';

class HomeScreen extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final drawerStatusController = useStreamController<DrawerStatus>();

    final drawerAnimation = useAnimationController(
      duration: Duration(milliseconds: 200),
      lowerBound: 0,
      upperBound: 1,
      initialValue: 0,
    );

    Matrix4 setTransform({double scale, double translate}) {
      return Matrix4.identity()
        ..scale(scale)
        ..translate(translate);
    }

    return StreamBuilder<DrawerStatus>(
      stream: drawerStatusController.stream,
      builder: (context, snapshot) {
        return Stack(
          children: [
            HomeDrawer(),
            AnimatedBuilder(
              animation: drawerAnimation,
              builder: (context, child) {
                var translate = 150.0 * drawerAnimation.value;
                var scale = 1 - (drawerAnimation.value * 0.25);
                var fade = (drawerAnimation.value - 2) * -0.5;
                var homeCorner = drawerAnimation.value * 30;

                return AnimatedOpacity(
                  duration: Duration(milliseconds: 200),
                  opacity: fade,
                  child: Transform(
                    alignment: Alignment.centerRight,
                    transform: setTransform(scale: scale, translate: translate),
                    child: ClipRRect(
                      borderRadius:
                          BorderRadius.all(Radius.circular(homeCorner)),
                      child: Home(
                        drawerStream: drawerStatusController,
                        drawerStatus: snapshot.data,
                        drawerAnimation: drawerAnimation,
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        );
      },
    );
  }
}

class Home extends HookWidget {
  final StreamController drawerStream;
  final DrawerStatus drawerStatus;
  final AnimationController drawerAnimation;

  Home({this.drawerStream, this.drawerStatus, this.drawerAnimation});
  @override
  Widget build(BuildContext context) {
    final animationCotroller = useAnimationController(
      lowerBound: 0,
      upperBound: 1,
      initialValue: 1,
      duration: Duration(
        milliseconds: 600,
      ),
    );
    final scrollController = useScrollController();
    final streamController = useStreamController<ScrollDirection>();
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.grey[50],
        actions: [
          HomeAppBar(
            drawerStream: drawerStream,
            drawerStatus: drawerStatus,
            drawerAnimation: drawerAnimation,
          ),
        ],
      ),
      body: Center(
          child: Column(
        children: [
          HomeSearchBox(),
          PetCategoryList(
            animationController: animationCotroller,
            streamController: streamController,
          ),
          NearByList(
            scrollController: scrollController,
            animationController: animationCotroller,
            streamController: streamController,
          ),
        ],
      )),
    );
  }
}
