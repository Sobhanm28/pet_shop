import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:pet_shop/ui/screen/discover/discover_widgets.dart';

List itemsLabel = [
  "Home",
  "Adoption",
  "Notification",
  "Profile",
  "Setting",
];

List itemsIcon = [
  Icons.home,
  Icons.pets,
  Icons.notifications_none,
  Icons.person,
  Icons.settings,
];

class HomeDrawer extends HookWidget {
  @override
  Widget build(BuildContext context) {
    print("BUild Home Drawer");
    return Scaffold(
      body: Container(
        width: width(1, context: context),
        height: height(1, context: context),
        color: Color(0xff212121),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: width(1, context: context),
              height: height(0.12, context: context),
              child: Row(
                children: [
                  Container(
                    width: 70,
                    height: 70,
                    margin: EdgeInsets.only(right: 20),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage("assets/images/prof.jpg"),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Sobhanm28",
                          style: TextStyle(color: Colors.white),
                        ),
                        Text(
                          "Sobhanm28@gmail.com",
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: height(0.4, context: context),
              width: width(1, context: context),
              margin: EdgeInsets.only(bottom: 150),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: List.generate(
                  5,
                  (index) {
                    return AnimationConfiguration.staggeredList(
                      position: index,
                      child: FadeInAnimation(
                        duration: Duration(milliseconds: 3500),
                        child: ListItem(
                          icon: itemsIcon[index],
                          label: itemsLabel[index],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            Container(
              child: ListItem(
                label: "Exit",
                icon: Icons.exit_to_app,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ListItem extends StatelessWidget {
  final String label;
  final IconData icon;
  const ListItem({
    Key key,
    this.label,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          margin: EdgeInsets.only(right: 15),
          child: Icon(
            icon,
            size: 25,
            color: Colors.white,
          ),
        ),
        Text(
          label,
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
      ],
    );
  }
}
