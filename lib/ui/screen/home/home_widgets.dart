import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:pet_shop/core/services/navigation_service.dart';
import 'package:pet_shop/ui/enums/enum.dart';
import 'package:pet_shop/ui/model/pet.dart';
import 'package:pet_shop/ui/screen/discover/discover_widgets.dart';

import '../../../locator.dart';

List categoriesName = [
  "Cat",
  "Dog",
  "Parrot",
  "Rabbit",
];

List categoriesImage = [
  "assets/images/cat_circle_cat.png",
  "assets/images/dog_circle_cat.png",
  "assets/images/parrot_circle_cat.png",
  "assets/images/rabbit_circle_cat.png",
];

int numberColor = 300;

List randomColors = [
  Colors.orange[numberColor],
  Colors.green[numberColor],
  Colors.indigo[numberColor],
  Colors.red[numberColor],
  Colors.purple[numberColor],
  Colors.amber[numberColor],
  Colors.cyan[numberColor],
  Colors.deepOrange[numberColor],
  Colors.lime[numberColor],
  Colors.deepOrange[numberColor],
];

List nearByListData = [
  {
    "breed": "Sweet pug",
    "describe": "they are protective for toy and food",
    "image": "assets/images/d3.png",
    "age": "2 year",
    "price": 18,
    "sex": "male",
    "about": "Lorem ipsum sit amet",
    "color": "Brown",
  },
  {
    "breed": "jack rusell",
    "describe": "jack rusell make good family pets.",
    "image": "assets/images/d2.png",
    "age": "1 year",
    "price": 38,
    "sex": "male",
    "about": "Lorem ipsum sit amet",
    "color": "Brown",
  },
  {
    "breed": "purebred Haski",
    "describe": "the abyssinian is loving and affectionate",
    "image": "assets/images/d4.png",
    "age": "2 year",
    "price": 85,
    "sex": "male",
    "about": "Lorem ipsum sit amet",
    "color": "Brown",
  },
  {
    "breed": "parrot",
    "describe": "Lorem ipsum sit amet",
    "image": "assets/images/p2.png",
    "age": "4 year",
    "price": 120,
    "sex": "male",
    "about": "Lorem ipsum sit amet",
    "color": "Brown",
  },
  {
    "breed": "Sweet Rabbit",
    "describe": "they are protective for toy and food",
    "image": "assets/images/r.png",
    "age": "2 year",
    "price": 8,
    "sex": "male",
    "about": "Lorem ipsum sit amet",
    "color": "Brown",
  },
  {
    "breed": "Owl",
    "describe": "jack rusell make good family pets.",
    "image": "assets/images/o.png",
    "age": "1 year",
    "price": 78,
    "sex": "male",
    "about": "Lorem ipsum sit amet",
    "color": "Brown",
  },
];

class HomeSearchBox extends StatelessWidget {
  const HomeSearchBox({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width(0.9, context: context),
      height: height(0.06, context: context),
      alignment: Alignment.center,
      margin: EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
          color: Colors.grey[100],
          borderRadius: BorderRadius.all(Radius.circular(35))),
      child: TextField(
        autofocus: false,
        decoration: InputDecoration(
          hintText: "Search Pets",
          border: InputBorder.none,
          suffixIcon: Icon(Icons.tune_sharp),
          prefixIcon: Icon(
            Icons.search,
            color: Colors.grey,
          ),
        ),
      ),
    );
  }
}

class HomeAppBar extends StatelessWidget {
  final StreamController drawerStream;
  final DrawerStatus drawerStatus;
  final AnimationController drawerAnimation;
  HomeAppBar({
    Key key,
    this.drawerStream,
    this.drawerStatus,
    this.drawerAnimation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      width: width(1, context: context),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () {
              if (drawerStatus == DrawerStatus.Open) {
                drawerAnimation.reverse();
                drawerStream.add(DrawerStatus.Close);
              } else {
                drawerAnimation.forward();

                drawerStream.add(DrawerStatus.Open);
              }
            },
            child: Icon(
              Entypo.list,
              color: Colors.grey,
            ),
          ),
          Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
                color: Colors.red,
                image: DecorationImage(
                    image: AssetImage("assets/images/prof.jpg"),
                    fit: BoxFit.cover),
                shape: BoxShape.circle),
          )
        ],
      ),
    );
  }
}

class PetCategoryList extends StatelessWidget {
  final StreamController<ScrollDirection> streamController;
  final AnimationController animationController;
  const PetCategoryList({
    Key key,
    this.streamController,
    this.animationController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: streamController.stream,
      initialData: ScrollDirection.forward,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return ScaleTransition(
          scale: animationController,
          child: FadeTransition(
            opacity: animationController,
            child: SizeTransition(
              sizeFactor: animationController,
              child: Container(
                padding: EdgeInsets.all(10),
                height: height(0.21, context: context),
                width: width(1, context: context),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Pet Category"),
                      AnimationLimiter(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Row(
                              children: List.generate(
                                categoriesImage.length,
                                (index) => AnimationConfiguration.staggeredList(
                                  duration: Duration(milliseconds: 800),
                                  position: index,
                                  child: FadeInAnimation(
                                    child: PetCategoryItem(
                                      index: index,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class PetCategoryItem extends StatelessWidget {
  final int index;
  const PetCategoryItem({Key key, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 5),
          width: 70,
          height: 70,
          decoration: BoxDecoration(
              color: randomColors[index],
              image: DecorationImage(
                  image: AssetImage(categoriesImage[index]), fit: BoxFit.cover),
              shape: BoxShape.circle),
        ),
        Text(categoriesName[index])
      ],
    );
  }
}

class NearByList extends StatelessWidget {
  final AnimationController animationController;
  final ScrollController scrollController;
  final StreamController<ScrollDirection> streamController;
  const NearByList({
    Key key,
    this.animationController,
    this.scrollController,
    this.streamController,
  }) : super(key: key);

  startEffect() {
    scrollController.addListener(() {
      switch (scrollController.position.userScrollDirection) {
        case ScrollDirection.forward:
          animationController.forward();
          streamController.add(ScrollDirection.forward);

          break;

        case ScrollDirection.reverse:
          animationController.reverse();
          streamController.add(ScrollDirection.reverse);

          break;
        default:
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    startEffect();
    return Expanded(
      flex: 25,
      child: Container(
        margin: EdgeInsets.only(
          top: 10,
        ),
        height: height(0.6, context: context),
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(flex: 1, child: Text("Near By")),
            Expanded(
              flex: 17,
              child: AnimationLimiter(
                child: ListView.builder(
                    controller: scrollController,
                    itemCount: nearByListData.length,
                    itemBuilder: (context, index) {
                      return AnimationConfiguration.staggeredList(
                        position: index,
                        duration: Duration(milliseconds: 800),
                        child: SlideAnimation(
                          horizontalOffset: 180.0,
                          child: FadeInAnimation(
                            child: NearByItem(
                              index: index,
                              pet: Pet.fromJson(nearByListData[index]),
                            ),
                          ),
                        ),
                      );
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NearByItem extends StatelessWidget {
  final int index;
  final Pet pet;
  NearByItem({
    Key key,
    this.index,
    this.pet,
  }) : super(key: key);

  double setRandomIndex(int i) => Random().nextInt(i).toDouble();

  List randomEmoji = [
    Icons.emoji_emotions,
    Icons.pets,
    Icons.emoji_nature,
  ];

  backgroundShape({
    List<double> positionFromLTRB,
    double size,
    IconData icon,
    Color iconColor,
    double rotateAngle,
  }) {
    return Positioned(
      left: positionFromLTRB[0],
      top: positionFromLTRB[1],
      right: positionFromLTRB[2],
      bottom: positionFromLTRB[3],
      child: Transform.rotate(
        angle: rotateAngle,
        child: Icon(
          icon,
          size: size,
          color: Colors.white38,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        locator<NavigationService>().pushNamed(
            routeName: "detail",
            args: {"pet": pet, "backgroundColor": randomColors[index]});
      },
      child: Hero(
        tag: pet.breed,
        child: Material(
          color: Colors.transparent,
          child: Container(
            margin: EdgeInsets.only(bottom: 10),
            height: height(0.2, context: context),
            width: width(1, context: context),
            decoration: BoxDecoration(
              color: randomColors[index],
              borderRadius: BorderRadius.all(
                Radius.circular(15),
              ),
            ),
            child: Stack(
              children: [
                backgroundShape(
                  icon: Icons.emoji_nature,
                  iconColor: Colors.white60,
                  positionFromLTRB: [310, 0, 0, 90],
                  size: 40,
                  rotateAngle: 0,
                ),
                backgroundShape(
                  icon: Icons.circle,
                  iconColor: Colors.white60,
                  positionFromLTRB: [0, 0, 210, 110],
                  size: 20,
                  rotateAngle: 0,
                ),
                backgroundShape(
                  icon: Icons.circle,
                  iconColor: Colors.white60,
                  positionFromLTRB: [100, 100, 0, 0],
                  size: 40,
                  rotateAngle: 0,
                ),
                backgroundShape(
                  icon: Icons.pets,
                  iconColor: Colors.white60,
                  positionFromLTRB: [0, 100, 120, 0],
                  size: 20,
                  rotateAngle: 0.5,
                ),
                backgroundShape(
                  icon: Icons.pets,
                  iconColor: Colors.white60,
                  positionFromLTRB: [0, 60, 40, 0],
                  size: 20,
                  rotateAngle: 0.5,
                ),
                backgroundShape(
                  icon: Icons.pets,
                  iconColor: Colors.white60,
                  positionFromLTRB: [0, 20, 80, 30],
                  size: 20,
                  rotateAngle: 0.5,
                ),
                backgroundShape(
                  icon: Icons.pets,
                  iconColor: Colors.white60,
                  positionFromLTRB: [20, 0, 0, 50],
                  size: 20,
                  rotateAngle: 0.5,
                ),
                backgroundShape(
                  icon: Icons.pets,
                  iconColor: Colors.white60,
                  positionFromLTRB: [0, 0, 0, 150],
                  size: 20,
                  rotateAngle: 0.5,
                ),
                backgroundShape(
                  icon: Icons.wb_sunny_outlined,
                  iconColor: Colors.white60,
                  positionFromLTRB: [0, 110, 260, 0],
                  size: 30,
                  rotateAngle: 0.5,
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          pet.breed,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          pet.describe,
                          style: TextStyle(color: Colors.white, fontSize: 13),
                        ),
                        Text("Age. 2 year"),
                        Text(
                          "\$" + pet.price.toString(),
                          style: TextStyle(fontSize: 18),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  right: 0,
                  bottom: 0,
                  top: 0,
                  child: Transform.scale(
                    scale: 1,
                    child: Container(
                      // color: Colors.red,
                      width: width(0.4, context: context),
                      height: height(1, context: context),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(pet.image),
                          fit: BoxFit.scaleDown,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
