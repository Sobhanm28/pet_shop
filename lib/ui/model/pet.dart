class Pet {
  final String breed;
  final String image;
  final String sex;
  final String about;
  final String describe;
  final String age;
  final String color;
  final int price;

  Pet({
    this.breed,
    this.image,
    this.describe,
    this.age,
    this.color,
    this.price,
    this.about,
    this.sex,
  });

  factory Pet.fromJson(Map<String, dynamic> json) {
    return Pet(
      breed: json["breed"],
      sex: json["sex"],
      about: json["about"],
      describe: json["describe"],
      image: json["image"],
      age: json["age"],
      price: json["price"],
      color: json["color"],
    );
  }
}
